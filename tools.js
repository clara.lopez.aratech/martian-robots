const __divideInPairs = (arrayCoordinates) => {
  let pairs = [];
  for (let i = 0; i < arrayCoordinates.length; i += 2) {
    pairs.push(arrayCoordinates.slice(i, i + 2));
  };
  return pairs;
};

const __readRobotInstructions = (instructionsArray, currentCoordinates, x, y, orientation, lastKnownPositions, marsOrientations, worldX, worldY, isLost) => {
  for (const instruction of instructionsArray) {
    currentCoordinates = x.toString().concat(' ', y, ' ', orientation);

    // **Responding to instructions**
    // Forward
    const myCoordinates = lastKnownPositions.includes(currentCoordinates);
    if (instruction === 'F' && myCoordinates === false) {
      const forwardResponse = __moveForward(x, y, orientation);
      x = forwardResponse.x;
      y = forwardResponse.y;
      orientation = forwardResponse.orientation;
      
      // Right
    } else if (instruction === 'R') {
      orientation = __rotateRight(marsOrientations, orientation);

      // Left
    } else if (instruction === 'L') {
      orientation = __rotateLeft(marsOrientations, orientation);
    }

    // Checking if coordenates are outside the rectangular world
    if (x > worldX || x < 0 || y > worldY || y < 0) {
      isLost = true;
      lastKnownPositions.push(currentCoordinates);
      break;
    }

  };
  return {
    x: x,
    y: y,
    orientation: orientation,
    isLost: isLost,
    currentCoordinates: currentCoordinates,
  };
};

const __finishRobotPosition = (isLost, lastRobotPosition, currentCoordinates, x, y, orientation, response) => {
  if (isLost) {
    lastRobotPosition = currentCoordinates.toString().concat(' LOST');
  } else {
    lastRobotPosition = x.toString().concat(' ', y, ' ', orientation);
  }
  response.push(lastRobotPosition);
};

const __moveForward = (x, y, orientation) => {
  switch (orientation) {
    case 'N':
      y += 1
      break;
    case 'E':
      x += 1
      break;
    case 'S':
      y -= 1
      break;
    case 'W':
      x -= 1
      break;
  };
  return {
    x: x,
    y: y,
    orientation: orientation,
  };
};

const __rotateRight = (marsOrientations, orientation) => {
  let currentOrientation = marsOrientations.indexOf(orientation);
  if (currentOrientation === 3) {
    orientation = marsOrientations[0];
  } else {
    orientation = marsOrientations[currentOrientation + 1];
  }
  return orientation;
};

const __rotateLeft = (marsOrientations, orientation) => {
  let currentOrientation = marsOrientations.indexOf(orientation);
  if (currentOrientation === 0) {
    orientation = marsOrientations[3];
  } else {
    orientation = marsOrientations[currentOrientation - 1];
  }
  return orientation;
};

module.exports = () => {
  return {
    readRobotInstructions: __readRobotInstructions,
    divideInPairs: __divideInPairs,
    finishRobotPosition: __finishRobotPosition
  }
};