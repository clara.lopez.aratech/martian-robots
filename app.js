const express = require('express')
const app = express()
const tools = require('./tools')();
const fs = require('fs');
let orientations = fs.readFileSync('orientation.json');

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/coordinates', function (req, res) {
  let coordinatesString = req.body.coordinates

  const response = [];
  let currentCoordinates = '';
  const lastKnownPositions = [];
  let marsOrientations = JSON.parse(orientations);

  // Coordinates array
  let arrayCoordinates = new Array();
  arrayCoordinates = coordinatesString.split('\n');

  // Rectangualar World size extrancted from array
  const rectangularWorld = arrayCoordinates[0].split(' ');
  const worldX = rectangularWorld[0];
  const worldY = rectangularWorld[1];
  arrayCoordinates.shift();

  // Data separeted in pairs
  const pairs = tools.divideInPairs(arrayCoordinates);

  for (const pair of pairs) {
    // Robot Position
    const robotInitPosition = pair[0];
    const position = robotInitPosition.split(' ');
    let x = Number(position[0]);
    let y = Number(position[1]);
    let orientation = position[2];
    let isLost = false;
    let lastRobotPosition;

    // Robot Instructions
    const robotInstructions = pair[1];
    let instructionsArray = robotInstructions.split('');

    const robotInstructionsResponse = tools.readRobotInstructions(instructionsArray, currentCoordinates, x, y, orientation, lastKnownPositions, marsOrientations, worldX, worldY, isLost);
    x = robotInstructionsResponse.x;
    y = robotInstructionsResponse.y;
    orientation = robotInstructionsResponse.orientation;
    isLost = robotInstructionsResponse.isLost;
    currentCoordinates = robotInstructionsResponse.currentCoordinates;

    // Adding robots' positions to an array
    tools.finishRobotPosition(isLost, lastRobotPosition, currentCoordinates, x, y, orientation, response);

  };
  res.send(response)
});


app.listen(3000, () => {
  console.log('Listening port 3000');
})